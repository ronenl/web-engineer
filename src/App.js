import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {
  constructor(props) {
      super(props);

      this.state = {
        name: '',
        region: ''
      };

      this.onInputChange = this.onInputChange.bind(this);
      this.onButtonClick = this.onButtonClick.bind(this);
  }

  // TODO change this implementation from static to dynamic
  buildLocationRows(locations) {
    const locationsArr = [];
    if (locations) {
      for (const locationKey in locations) {
        if (locations[locationKey].name && locations[locationKey].region) {
          locationsArr.push(
            <tr key={locationKey}><td>{locations[locationKey].name}</td><td>{locations[locationKey].region}</td></tr>,
          );
        }
      }
    }
    return locationsArr;
  };

  onInputChange (e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onButtonClick() {
    const {name, region} = this.state;
    this.props.addLocation({ name, region })
  }


  // TODO change the implementation of the add_location button to retrieve the name and region via form input elements
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Elminda</h2>
        </div>

      <button id="add_location"
        onClick={this.onButtonClick}
        >
        Add Name and Region
      </button>

      <table>
        <thead>
          <tr><th>Name</th><th>Region</th></tr>
        </thead>
        <tbody>
          { this.buildLocationRows(this.props.locations) }
        </tbody>
      </table>
      <div><label>Enter your name: <input type="text" name="name" onChange={this.onInputChange} /></label></div>
      <div><label>Enter your region: <input type="text" name="region" onChange={this.onInputChange} /></label></div>
      </div>
    );
  }
}

export default App;
